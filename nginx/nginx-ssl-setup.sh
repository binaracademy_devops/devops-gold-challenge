#!/bin/bash
sudo apt update
sudo apt install nginx
cd /etc/nginx
sudo mkdir ssl
cd /etc/nginx/ssl
sudo wget https://gitlab.com/roboticpuppies/ssl-certificate/-/raw/main/cert1.pem
sudo wget https://gitlab.com/roboticpuppies/ssl-certificate/-/raw/main/privkey1.pem
cd ..
sudo wget $NGINX_CONF_FILE_URL -O nginx.conf
sudo nginx -t && sudo systemctl reload nginx