# The App

Aplikasi Profile CRUD sederhana, yang dibangun dengan ReactJS, ExpressJS, dan PostgreSQL.

```
1. Production    : https://andre.servercare.id/
2. Development   : https://dev-andre.servercare.id/
```

# Docker Registry

* [Frontend](https://hub.docker.com/repository/docker/muhandre/bdw2_gc_fe/general)

* [Backend](https://hub.docker.com/repository/docker/muhandre/bdw2_gc_be/general)

# PostgreSQL Initialization Query

```sql
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  name varchar(30),
  email varchar(30),
  gender varchar(6)
);

INSERT INTO users (name, email, gender) VALUES
('John Doe', 'john@gmail.com', 'Male'),
('Mark Lee', 'mlee@gmail.com', 'Male'),
('Sofia', 'sofia@gmail.com', 'Female'),
('Michelle', 'mangela@gmail.com', 'Female');

exit
```

# Deployment

### Note

* Flow yang dituliskan di bawah ini, ditujukan sebagai penjelasan implementasi diri terhadap challenge tersebut. 

* Dengan begitu, apabila hendak ada yang mencoba challenge ini, perlu disesuaikan beberapa hal.

### Flow
1. Buat file Dockerfile dan file .dockerignore untuk Frontend dan Backend.

2. Buat Docker Registry untuk menampung Frontend docker image dan Backend docker image.

3. Buat docker-compose-development.yaml untuk development server dan docker-compose.yaml untuk production server.

4. Buat nginx.conf untuk production server dan development server.

5. Buat script otomasi setup NGINX dan SSL. 

6. *Set* environtment variable yang diperlukan oleh masing-masing file docker compose. 

Environtment variable saya set pada Gitlab Project Environtment Variable. Hal ini bertujuan untuk me-*replace* file `<prod/dev>.env.example` pada root directory menggunakan *command* `envsubst` saat berlangsungnya CI/CD.

Pada challenge ini, terdapat ketentuan terhadap beberapa environtment variable, yakni

```
DATABASE_USERNAME=people
DATABASE_PASSWORD=people
DATABASE_DATABASE=people
BACKEND_PORT=8080
DATABASE_PORT=5432
```

Untuk environtment `<PRODUCTION/DEVELOPMENT>_BACKEND_URL` saya set dengan subdomain yang diberikan oleh Fasilitator.

Sedangkan untuk environtment `<PRODUCTION/DEVELOPMENT>_DATABASE_HOST` saya *set* dengan private IP daripada public IP. Hal ini untuk mengurangi *bottleneck*.

* PRODUCTION saya set dengan private IP GCP Cloud SQL
* DEVELOPMENT saya set dengan private IP VM terkait

7. Sediakan satu production compute engine instance dan satu development compute engine instance pada GCP.

**NOTE:** Demi keamanan, dan berhubung challenge ini menerapkan SSL, firewall ingress yang dibuka hanya `allow-https`

8. Buat dua buah *static IP* agar *public IP* dari masing-masing server tidak berubah jika sempat di-*stop*.

9. Simpan *static IP* dari production server dan development server, masing-masing sebagai value dari Gitlab Project Environtment Variable `PRODUCTION_HOST_IP` dan `DEVELOPMENT_HOST_IP`

10. Sediakan satu Cloud SQL instance pada GCP.

**NOTE:** Demi keamanan, connection yang dibuka hanyalah private connection.

11. Buat user bernamakan `people`, dengan password `people`, lalu buat database `people` pada Cloud SQL instance tersebut.

Inisialisasi table dengan query yang telah disebutkan.

12. Install docker package pada kedua server tersebut. 
Jika OS kedua server tersebut adalah Ubuntu, maka instalasi dapat mudah dilakukan dengan script [ini](https://raw.githubusercontent.com/nandomardi23/bash-script-automate/main/install-docker.sh).

13. Ubah permission file tersebut agar bisa dieksekusi sebagai binary dengan `chmod +x <file path>`.

14. Eksekusi script tersebut.

15. Set environtment variable `NGINX_CONF_FILE_URL` pada masing-masing server melalui SSH, dengan value URL raw file nginx.conf. 

Value nya adalah URL-URL berikut.
* [Production nginx.conf](https://gitlab.com/binaracademy_devops/devops-gold-challenge/-/raw/master/nginx/prod/nginx.conf)
* [Development nginx.conf](https://gitlab.com/binaracademy_devops/devops-gold-challenge/-/raw/master/nginx/dev/nginx.conf)

Command untuk menjalankannya adalah sebagai berikut `export NGINX_CONF_FILE_URL=<URL nginx.conf raw file>`.

16. `wget` script [ini](https://gitlab.com/binaracademy_devops/devops-gold-challenge/-/raw/master/nginx/nginx-ssl-setup.sh) untuk setup NGINX dan SSL pada masing-masing server.

Setelah di-*download*, lakukan langkah **13** dan **14**

17. Siapkan sepasang public dan private key untuk Gitlab Runner SSH kepada server.

18. Private key tersebut jadikan sebagai value dari Gitlab Environtment Variable `SSH_HOST_PRIVATE_KEY`.

19. *Set* semua envirotnment variable yang belum dan dibutuhkan oleh `gitlab-ci.yaml` yang hendak dibuat.

20. Buatkan pipeline CI/CD dengan cara membuat instruksinya pada file `gitlab-ci.yaml`

21. Setelah sekali deployment, SSH ke development server.

Kemudian setelah SSH, jalin koneksi terhadap database melalui command berikut.

`psql -p 5432 -h localhost -U people -W people`

**NOTE**: Install package yang diperlukan jika tidak dapat menjalankan command tersebut.

Setelah itu, inisialisasi table dengan query yang telah disebutkan.

22. Lakukan Iterasi Software Development.

# Local setup without docker

### database

Siapkan
* user  `people` dengan password `people`
* database `people`
* Port 5432 jika belum digunakan, atau port lain jika sudah digunakan. 

**NOTE:** Jangan lupa .env.example backend disesuaikan jika port nya bukan 5432.

### Backend

Buat .env file. Sesuaikan kembali nilai .env jika perlu.

```
cd backend
cp .env.example .env
```
Install setiap dependencies, lalu jalankan.
```
npm install
npm start
```

### Frontend

Buat .env file. Sesuaikan kembali nilai .env jika perlu.

```
cd frontend
cp .env.example .env
```

Install setiap dependencies, lalu jalankan.
```
npm install
npm start
```

# Local setup dengan docker

1. Pastikan docker ter-*install* pada komputer Anda.

2. Build docker image untuk masing-masing backend dan frontend dengan format tag `muhandre/bdw2_gc_<be/fe>`

3. Buat file .env di *root directory* yang isinya merupakan gabungan env.example frontend dan backend.

4. Buat docker-compose-local.yaml. 

File ini bisa dapat mudah dibuat dengan sedikit modifikasi dari docker-compose-development.yaml. Modifikasi yang perlu dilakukan adalah sebagai berikut.

* Hapus sufiks `_${COMMIT_SHA}` pada container name.
* Ganti `${IMAGE_TAG}` dengan tag image yang baru saja di-*build* secara manual.
* Ganti 
  * `${BACKEND_PORT}` menjadi `${BASE_URL_PORT}` 
  * `${DEVELOPMENT_DATABASE_HOST}` menjadi `${DATABASE_HOST}`
  * `${DEVELOPMENT_BACKEND_URL}` menjadi `${REACT_APP_BACKEND_URL}`

Setelah itu, jalankan aplikasi dengan `docker compose -f docker-compose-local.yaml up -d --force-recreate` 

# Credit

All credit goes to [M. Fikri](https://www.youtube.com/watch?v=es9_6RFR7wk&t=3336s) as creator of this app.

App used:

[Frontend](https://github.com/mfikricom/Frontend-React-MySQL)
[Backend](https://github.com/mfikricom/Backend-API-Express-MySQL)